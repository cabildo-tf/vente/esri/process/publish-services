# Publish services

Publish services es un script de python que permite crear servicios en ArcGIS Enterprise 
usando un proyecto de ArcGIS Pro en combinación un un fichero YAML donde se definen los
metadatos y tipos de servicios a crear.

## Estructura fichero YAML
El fichero de definición de los servicios sigue la siguiente estrucutra, donde hay una parte genérica de
metadatos y luego el listado de servicios que se desean publicar. 
```yaml
properties: &generic_properties
    serviceName:
    description: My Description
    summary: My Summary
    tags:
      - My Tags
    credits: My Credits
    useLimitations: My Use Limitations
    portalFolder:
    offline:
    overwriteExistingService:
   
services:
    - type: FEATURE
      server_type: HOSTING_SERVER | FEDERATED_SERVER
	  layers_and_tables:
        -
      properties: 
        <<: *generic_properties
	    allowExporting:

    - type: TILE
      server_type: HOSTING_SERVER | FEDERATED_SERVER
      layers_and_tables:
        -
      properties:
        <<: *generic_properties
  
    - type: MAP_IMAGE
      server_type: HOSTING_SERVER | FEDERATED_SERVER
	  layers_and_tables:
        -
      properties: 
        <<: *generic_properties
	    copyDataToServer:
		federatedServerUrl:
		serverFolder:      
```
## Ejecución
Para lanzar el script es necesario instalar el paquete en un entorno con ArcPy, 
a continuación ejecutar el siguiente comando con los parámetros adecuados. 

```shell script
publish-services -a <path_to_aprx> -c <path_to_config_yml> -P <portal_url> -p <password> -u <username> 
```
| Parámetro         | Descripción                                                                        | Valor por defecto      |
|-------------------|------------------------------------------------------------------------------------|------------------------|
| a, aprx           | Ruta al fichero del proyecto del ArcGIS Pro                                        |                        |
| c, service-config | Ruta al fichero con la configuración                                               |                        |
| P, portal-url     | Url al portal                                                                      | https://www.arcgis.com |
| u, username       | Usuario del portal                                                                 |                        |
| p, password       | Password del usuario                                                               |                        |
| l, logging        | Ruta al fichero con la configuración del log                                       | logging.yml            |
| o, output         | Ruta al directorio donde se almacenaran los ficheros de la definición del servicio | output                 |