import logging.config
import os

import yaml


class SetupLogging:
    def __init__(self, *args, **kwargs):
        root_path = os.path.dirname(__file__)
        self.default_config = os.path.join(root_path, kwargs.pop("logging_path"))

    def setup_logging(self, default_level=logging.INFO, env_key='LOG_CFG'):
        path = os.getenv(env_key, self.default_config)
        if os.path.exists(path):
            with open(path, 'r') as f:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
        else:
            logging.info('Error in loading logging configuration. Using default configs')
            logging.basicConfig(level=default_level)
