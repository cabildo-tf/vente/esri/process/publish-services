import argparse
import logging
import os
from pathlib import Path

import arcpy
import yaml
from ags_conf_validator.validator import validate

from publish_services.logging.setup_logging import SetupLogging


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--aprx", dest="aprx_path",
                        help="ArcGIS Pro project path", required=True)
    parser.add_argument("-c", "--service-config", dest="service_conf_path", required=True,
                        help="Service configuration path", default="service.yml")
    parser.add_argument("-P", "--portal-url", dest="portal_url",
                        help="Portal for ArcGIS url", default="https://www.arcgis.com")
    parser.add_argument("-u", "--username", dest="username", help="Username", required=True)
    parser.add_argument("-p", "--password", dest="password", help="Password", required=True)
    parser.add_argument("-l", "--logging", dest="logging_conf_path", default="logging.yml")
    parser.add_argument("-o", "--output", dest="output_path", default="output")
    args = parser.parse_args()

    SetupLogging(logging_path=args.logging_conf_path).setup_logging()

    logging.info("Starting")
    logging.info("Loading configuration")
    validate(config=args.service_config_path)
    sevice_conf = load_config(args.service_conf_path)
    aprx = load_aprx(args.aprx_path)
    output_path = get_output_path(args.output_path)
    portal_conf = extract_portal_config(args)
    publish_service(aprx, sevice_conf, output_path, portal_conf)

    logging.info("Completed")


def load_aprx(path):
    logging.info(f"Loading file {path}")
    aprx = arcpy.mp.ArcGISProject(path)
    logging.info("ArcGIS Pro file loaded")
    return aprx


def get_output_path(folder):
    path = os.path.join(os.getcwd(), folder)
    logging.info(f"Creating output folder '{path}'")
    Path(path).mkdir(exist_ok=True)
    return path


def load_config(path):
    if path and os.path.exists(path):
        with open(path, 'r') as f:
            return yaml.safe_load(f.read())


def extract_portal_config(args):
    portal_args = dict((k, getattr(args, k)) for k in ('portal_url', 'username', 'password'))
    return portal_args


def publish_service(aprx, config, output_path, portal_conf):
    for map in aprx.listMaps():
        for service in config['services']:
            service = prepare_service_conf(service)
            sddraft, sddraft_filename = export_sharing_draft(map, service, output_path)
            sd_filename = generate_service_definition(service, sddraft_filename, output_path)
            upload_service_definition(sd_filename, portal_conf)


def prepare_service_conf(service):
    service['service_name'] = service['properties']['serviceName']
    return service


def generate_filename(output_path, expression, obj):
    return os.path.join(output_path, expression.format(**obj))


def export_sharing_draft(map, service, output_path):
    logging.info("Exporting service definition draft of '{service_name}'".format(**service))
    sddraft = map.getWebLayerSharingDraft(service['server_type'], service['type'],
                                          service['properties']['serviceName'],
                                          service['layers_and_tables'])
    set_sharing_draft_properties(sddraft, service['properties'])

    sddraft_filename = generate_filename(output_path, "{type}_SD_{service_name}.sddraft", service)

    if os.path.exists(sddraft_filename):
        os.remove(sddraft_filename)

    sddraft.exportToSDDraft(sddraft_filename)

    logging.info("Service definition draft exported")
    return sddraft, sddraft_filename


def set_sharing_draft_properties(sharing_draft, values):
    for field_name, value in values.items():
        if field_name == 'tags':
            value = ';'.join(value)
        setattr(sharing_draft, field_name, value)

    return sharing_draft


def generate_service_definition(service, sddraft_filename, output_path):
    logging.info("Exporting service definition of '{service_name}'".format(**service))
    sd_filename = generate_filename(output_path, "{type}_SD_{service_name}.sd", service)
    if os.path.exists(sd_filename):
        os.remove(sd_filename)

    arcpy.StageService_server(sddraft_filename, sd_filename)
    logging.info("Service definition exported")
    return sd_filename


def upload_service_definition(sd_filename, portal_conf):
    logging.info("Uploading service definition to {portal_url}".format(**portal_conf))
    arcpy.SignInToPortal(**portal_conf)
    arcpy.UploadServiceDefinition_server(sd_filename, portal_conf["portal_url"])
    logging.info("Service uploaded")


if __name__ == '__main__':
    main()
