import setuptools

setuptools.setup(
    name="publish_services",
    version="0.0.2",
    author="Ignacio Lorenzo",
    author_email="nacholore@gmail.com",
    description="Publish services ArcGIS Pro",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=[],
    entry_points={
        'console_scripts': ['publish-services=publish_services.main:main'],
    }
)
